import Head from 'next/head'
import Header from '../components/Header'
import Joinbutton from '../components/Joinbutton'

export default function Home() {
  return (
    <div style=
            {{
                overflowX: 'hidden',
                display: 'block',
            }}
            className="min-w-screen absolute w-full">
            <Head>
                <title>Larkline App</title>
                <meta name='keywords' content='event planning, sports, intramurals, recreation, recreation' />
            </Head>
            <div 
            style={{
                background: `url('/mainLarkline.svg') no-repeat center `,
                backgroundSize: 'contain',
                height: '100vh',
                position: 'relative',
                backgroundColor: '#434343',
                }}>
                <div style=
                    {{
                        maxWidth: '10rem',
                        margin: '0 auto 0 auto',
                        padding: '45vh 20px 20px 20px',
                        background: 'none',
                        position: 'relative',
                    }}>
                    <Joinbutton />
                </div>
            </div>
    </div>
  )
}

