import axios from "axios";

function getRequestParams(email, zipCode) {
    //get env variables
    const API_KEY = process.env.REACT_APP_MAILCHIMP_API_KEY;
    const LIST_ID = process.env.REACT_APP_MAILCHIMP_LIST_ID;

    var DATACENTER = API_KEY.split("-")[1];
    const url = `https://${DATACENTER}.api.mailchimp.com/3.0/lists/${LIST_ID}/members`;

    const data = {
        email_address: email,
        status: "subscribed",
        merge_fields: {
            ZIP: zipCode,
        }
    };

    console.log('==data', data);

    const base64ApiKey = Buffer.from(`anystring:${API_KEY}`).toString("base64");
    const headers = {
        "Content-Type": "application/json",
        Authorization: `Basic ${base64ApiKey}`,
    };

    return {
        url,
        data,
        headers,
    };
} 

export default async (req, res) => {
    const { email, zipCode } = req.body;
    
    if (!email || !email.length) {
        return res.status(400).json({
            error: "Forgot to add your email?",
        });
    } 

    try {
        const { url, data, headers } = getRequestParams(email, zipCode);
        console.log("== headers", headers);
        const response = await axios.post(url, data, { headers });
        console.log("==response: ", response);
        // Success
        return res.status(201).json({ error: null })
    } catch (error) {
        console.log("=error",error);
        return res.status(400).json({
            error: `Oops, something went wrong... Send us an email at larklineapp@gmail.com and we'll add you to the list.`
        });
    }
}