import Head from 'next/head'
import Newsletter from '../components/Newsletter'

const app = () => {
    return (
        <div style=
            {{
                overflowX: 'hidden',
                display: 'block',
            }}
            className="min-w-screen absolute w-full">
            <Head>
                <title>Larkline App</title>
                <meta name='keywords' content='event planning, sports, intramurals, recreation, recreation' />
            </Head>
            <div 
            style={{
                background: `url('/appLarkline.svg') no-repeat center `,
                backgroundSize: 'contain',
                height: '100vh',
                position: 'relative',
                backgroundColor: '#434343',
                }}>
                <div style=
                    {{
                        width: '400px',
                        margin: '0 auto 0 auto',
                        padding: '30vh 20px 20px 20px',
                        background: 'none',
                        position: 'relative',
                    }}>
                    <Newsletter />
                </div>
            </div>           
        </div>
    )
}

export default app