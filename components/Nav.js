import Link from 'next/link'
import { useState } from 'react'
import navStyles from '../styles/Nav.module.css'

const Nav = () => {
    const [isOpen, setOpen] = useState(false);
    const handleOpen = () => setOpen(!isOpen);
    
    return (
        <nav className="bg-yellow-600 sm:flex sm:items-center sm:justify-between sm:px-4 sm:py-3">
            <div className="flex items-center justify-between px-4 py-3 sm:p-0">
                <div>
                    <Link href="/" ><img className="h-8" src="/img/" alt="Larkline" /></Link>
                </div>
                <div className="sm:hidden">
                    <button onClick={handleOpen} type="button" className="block text-gray-500 hover:text-white focus:outline-none">
                        <svg className="h-6 w-6 fill-current" viewBox="0 0 24 24">
                            {isOpen 
                                ? <path v-if="isOpen" fillRule="evenodd" d="M18.278 16.864a1 1 0 0 1-1.414 1.414l-4.829-4.828-4.828 4.828a1 1 0 0 1-1.414-1.414l4.828-4.829-4.828-4.828a1 1 0 0 1 1.414-1.414l4.829 4.828 4.828-4.828a1 1 0 1 1 1.414 1.414l-4.828 4.829 4.828 4.828z"/> 
                                : <path fillRule="evenodd" d="M4 5h16a1 1 0 0 1 0 2H4a1 1 0 1 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2z"/>
                            }
                        </svg>
                    </button>
                </div>
            </div>
            <div className={`px-2 pt-2 pb-4 sm:flex  ${isOpen ? 'block' : 'hidden'}`}>
                <Link href='/host'><a className="block py-1 font-semibold hover:bg-gray-800 rounded px-2">HOST</a></Link>
                <Link href='/team'><a className="block py-1 mt-1 font-semibold hover:bg-gray-800 rounded px-2 sm:mt-0 sm:ml-2">TEAM</a></Link>
                <Link href='/app'><a className="block py-1 mt-1 font-semibold hover:bg-gray-800 rounded px-2 sm:mt-0 sm:ml-2">APP</a></Link>
                <Link href='/faq'><a className="block py-1 mt-1 font-semibold hover:bg-gray-800 rounded px-2 sm:mt-0 sm:ml-2">FAQ</a></Link>
            </div>     
        </nav>
    )
}

export default Nav
