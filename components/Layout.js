import Nav from './Nav'
import Header from './Header'


const Layout = ({children}) => {
    return (
        <>
            {/* <Nav /> */}
            <div className="bg-white ">
                <main>
                    {children}
                </main>
            </div>
        </>
    )
}

export default Layout
