import { useState } from 'react'
import axios from 'axios'

const Newsletter = () => {
    const [email, setEmail] = useState('');
    const [zipCode, setZipCode] = useState('');
    const [state, setState] = useState('IDLE');
    const [errorMessage, setErrorMessage] = useState(null);

    const join = async () => {
        setState('LOADING');
        setErrorMessage(null);
        try {
            const response = await axios.post('/api/newsletter', { email, zipCode });
            console.log("==response: ", response);
            setState('SUCCESS');
        } catch (e) {
            setErrorMessage(e.response.data.error);
            setState('ERROR');
        }
    };

    return (
        <div className="grid bg-lblack grid-cols-1 auto-col-max items-center justify-center w-full p-8 border-white border-solid border rounded-sm">
            <div className="my-2">
                <h2 className="text-3xl font-bold text-center text-white">
                    Join the list!
                </h2>
            </div>
            <div className="my-1">
                <input
                    className="appearance-none mb-2 lg:mb-0 w-full lg:w-full shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline border-gray-500 rounded py-2 px-4 text-center"
                    type="text"
                    placeholder="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
            </div>
            <div className="my-1">
                <input
                    className="appearance-none mb-2 lg:mb-0 w-full lg:w-full shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline border-gray-500 rounded py-2 px-4 text-center"
                    type="text"
                    placeholder="Zip Code"
                    value={zipCode}
                    onChange={(e) => setZipCode(e.target.value)}
                />
            </div>
            <div className="my-2 mx-auto lg:w-1/2 md:w-1/3">
                <button
                    className={`w-full shadow bg-brand2 focus:shadow-outline focus:outline-none text-center bg-lorange hover:bg-blue-700 text-white font-bold py-2 px-4 rounded ${
                        state === 'LOADING' ? "button-gradient-loading" : ""
                    }`}
                    type="button"
                    disabled={state === 'LOADING'}
                    onClick={join}
                >
                    Join
                </button>
            </div>
            <div className="text-center mx-auto lg:w-1/2 md:w-1/3">
            {state === 'ERROR' && (
                <p className="text-red-600 w-full">{errorMessage}</p>
            )}
            {state === 'SUCCESS' && (
                <p className="font-bold text-green-600">Success!</p>
            )}
            </div>
        </div>
    );
};

export default Newsletter;