import Link from 'next/link'
import buttonStyles from '../styles/Joinbutton.module.css'

export default function Joinbutton() {
    return (
        <Link href="app" as={`/app`}>
            <button type="button" className={buttonStyles.btn}>
                <span>SIGN UP</span>
            </button>
        </Link>
    )
}
