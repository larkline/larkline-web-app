import headerStyles from '../styles/Header.module.css'

const Header = () => {
    return (
        <div>
            <h1 className={headerStyles.title}>
                Welcome to <span className="uppercase">Larkline</span>
            </h1>
            <p className={headerStyles.description}>
                Join or host your own public sports games and athletic activities.
            </p>
        </div>
        
    )
}

export default Header
