const colors = require('tailwindcss/colors')

module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    inset: {
      '0': 0,
      auto: 'auto',
      '1/2': '47%',
      '1': "5%",
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.trueGray,
      indigo: colors.indigo,
      red: colors.rose,
      yellow: colors.amber,
      lorange: '#FA6B05',
      lblack: '#434343',
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
